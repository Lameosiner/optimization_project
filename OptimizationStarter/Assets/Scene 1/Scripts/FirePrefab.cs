﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Ensure that there is a Camera attached to this object
[RequireComponent(typeof(Camera))]
public class FirePrefab : MonoBehaviour
{


    //The prefab to be instantiated
    public GameObject Prefab;

    //Speed that the prefab will travel at once it is fired
    public float FireSpeed = 5;

    // Update is called once per frame
    void Update()
    {
        //Once there is input (as mapped in the Input settings)
        if (Input.GetButton("Fire1"))
        {
            //Get the point that the mouse is clicked at based on the camera's frame
            Vector3 clickPoint = GetComponent<Camera>().ScreenToWorldPoint(Input.mousePosition + Vector3.forward);
            //Set "FireDirection" to this position and normalize it
            Vector3 FireDirection = clickPoint - this.transform.position;
            FireDirection.Normalize();
            //Instantiate "Prefab" at the position of this object
            GameObject prefabInstance = GameObject.Instantiate(Prefab, this.transform.position, Quaternion.identity, null);
            //Access the rigidbody on the instantiated prefab and set its velocity so it moves towards FireDirection at a speed of FireSpeed
            prefabInstance.GetComponent<Rigidbody>().velocity = FireDirection * FireSpeed;

            StartCoroutine(DestroyThisObject(prefabInstance));
        }
    }

    IEnumerator DestroyThisObject(GameObject firedObject)
    {
        yield return new WaitForSeconds(3);

        Destroy(firedObject);
    }

}
