﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ClickToSetNavTarget : MonoBehaviour
{
    
	// Update is called once per frame
	void Update ()
    {
        //Find the Main Camera in the scene and access its Camera component
        Camera camera = GameObject.Find( "Main Camera" ).GetComponent<Camera>();
        RaycastHit hit = new RaycastHit();

        //Raycast out from the camera, selectively targeting the "Ground" layer
        //If the raycast hits "ground", set the target destination of this navmesh agent to the raycast hit position
        if ( Physics.Raycast( camera.ScreenPointToRay( Input.mousePosition ), out hit, float.MaxValue, LayerMask.GetMask( "Ground" ) ) )
        {
            GetComponent<NavMeshAgent>().destination = hit.point;
        }
	}
}
