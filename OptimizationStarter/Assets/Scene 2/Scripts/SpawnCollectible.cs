﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class SpawnCollectible : MonoBehaviour
{

    public static SpawnCollectible instance;

    //The object to be instantiated
    public GameObject Collectible;

    [SerializeField]
    //The instance
    public GameObject m_currentCollectible;


    void Awake()
    {
        instance = this;

        SpawnPickup();
    }

    //// Update is called once per frame
    //void Update()
    //{
    //    //If there is no currently instantiated collectible, instantiate one
    //    if ( m_currentCollectible == null )
    //    {
    //        //Instantiate a Collectible at the position of a randomly selected child object of this gameObject
    //        m_currentCollectible = Instantiate( Collectible, this.transform.GetChild( UnityEngine.Random.Range( 0, this.transform.childCount ) ).position, Collectible.transform.rotation );
    //    }
    //}

    public void SpawnPickup()
    {
        if (m_currentCollectible == null)
        {
            //Instantiate a Collectible at the position of a randomly selected child object of this gameObject
            m_currentCollectible = Instantiate(Collectible, this.transform.GetChild(UnityEngine.Random.Range(0, this.transform.childCount)).position, Collectible.transform.rotation);
        }
    }


}
