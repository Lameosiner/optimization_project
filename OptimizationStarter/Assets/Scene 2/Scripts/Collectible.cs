﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectible : MonoBehaviour
{
    public float Radius;

    public void Update()
    {
        //Create an array of colliders to store the colliders collected by the overlapShere
        Collider[] collidingColliders = Physics.OverlapSphere( this.transform.position, Radius );
        //Iterate through each index of the array
        for ( int colliderIndex = 0; colliderIndex < collidingColliders.Length; ++colliderIndex )
        {
            //If any object tagged "Player" is found in the array, destroy it
            if ( collidingColliders[colliderIndex].tag == "Player" )
            {
                SpawnCollectible.instance.m_currentCollectible = null;
                SpawnCollectible.instance.SpawnPickup();

                Destroy( this.gameObject );

            }
        }
    }

}
